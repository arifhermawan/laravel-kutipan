<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Quote;
use App\Models\User;
use Auth;
use App;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $quotes = Quote::all();
      return view('quotes.index')->with('quotes', $quotes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('quotes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // check if ready exists or not;
      $slug = str_slug($request->title, '-');

      // cek slug apakah ada kembar / tidak
      if (Quote::where('slug', $slug)->first()  != null ) {
        $slug = $slug. '-' .time();
      }

      // Validasi input
      $this->validate($request, [
        'title' => 'required | min:5',
        'subject' => 'required | min:5'
      ]);

      $quotes = Quote::create([
        'title' => $request->title,
        'slug'  => $slug,
        'subject' => $request->subject,
        'user_id' => Auth::user()->id
      ]);

      // redirect view
      return redirect('quotes')->with('msg', 'Kutipan berhasil di buat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
      $quote = Quote::where('slug', $slug)->first();

      if (empty($quote)) {
        App::abort(404, 'message');
      }

      return view('quotes.single')->with('quote', $quote);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
