@extends('layouts.app')

@section('content')
<div class="container">

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form class="" action="/quotes" method="post">
    {{ csrf_field() }}

    <div class="form-group">
      <label for="title">Judul</label>
      <input type="text" name="title" value="{{ old('title') }}" class="form-control" id="title" placeholder="tulis judul di sini">

      <label for="subject">Subject</label>
      <textarea name="subject" rows="8" cols="80" class="form-control" id="subject">{{ old('subject') }}</textarea>
      <br>

      <button type="submit" name="button" class="btn btn-success btn-block">Submit</button>
    </div>
  </form>
</div>
@endsection
